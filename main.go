/**
 * Rebuilds a binary file described from a HEX dump.
 *
 * Takes the hex dump in stdin and writes the file out to stdout
 */

package main

import (
    "os"
    "flag"
    "log"
    "strconv"
)

var flagStartOffset *string = flag.String("s", "", "Offset to start writing")

func main() {
    var output WriterWithOffset = &IOWriter{os.Stdout}

    flag.Parse()

    if *flagStartOffset != "" {
        startingOffset, err := strconv.ParseUint(*flagStartOffset, 0, 64)
        if err != nil {
            log.Fatal(err)
        }

        output = &LowerGateWriter{output, startingOffset}
    }

    scanner := NewStandardHexFormatScanner(os.Stdin)
    err := WriteHexTo(scanner, output)

    if err != nil {
        log.Fatal(err)
    }
}