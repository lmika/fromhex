package main

import (
    "io"
    "bufio"
    "errors"
    "unicode"
    "strings"
    "strconv"
)


// Error returned if a blank line is encountered.
var ErrBlankLine = errors.New("Blank line")

// Insufficient number of columns in input line
var ErrInsufficientCols = errors.New("Insufficient cols")


// Interface for something that can read lines from a hex dump and interpret
// it as offset and data.
type HexFormatScanner interface {

    // Scans the next line.  Returns true if the next line is available, or
    // false if the EOF or an error occurs
    Scan()  bool

    // Returns the scanned offset
    Offset() uint64

    // Returns the scanned data
    Bytes() []byte

    // Returns any errors encountered.  If Scan() returns false, UNLESS the EOF has
    // been reached, this method returns the error encounted.
    Err()   error
}

// The standard hex format scanner.  This accepts a hex dump of the form:
//  
//      <offsetInHex>   <binDataInHex>*16  <asciiData>
//
type stdHexFormatScanner struct {
    scanner     *bufio.Scanner

    hexCols     int     // Number of hex columns in the input

    lastOffset  uint64
    lastData    []byte
    lastErr     error
}

func NewStandardHexFormatScanner(r io.Reader) HexFormatScanner {
    return &stdHexFormatScanner{
        scanner: bufio.NewScanner(r),
        hexCols: 16,
    }
}

// Scans the next line
func (fs *stdHexFormatScanner) Scan() bool {
    for {
        hasScanned, line := fs.scanLine()
        if !hasScanned {
            return false
        }

        err := fs.interpretLine(line)

        // If no error all good
        if err == nil {
            return true
        } else if err != ErrBlankLine {
            fs.lastErr = err
            return false
        }

        // Otherwise, the error is a blank line.  Simply scan the next line
    }

    // Will never reach this
    return false
}

// Returns the scanned offset
func (fs *stdHexFormatScanner) Offset() uint64 {
    return fs.lastOffset
}

// Returns the scanned data
func (fs *stdHexFormatScanner) Bytes() []byte {
    return fs.lastData
}

// Returns the error
func (fs *stdHexFormatScanner) Err() error {
    return fs.lastErr
}

// Call the scan function and set the last error value.  Returns the result
// of scanning, and the line.
func (fs *stdHexFormatScanner) scanLine() (bool, string) {
    hasScanned := fs.scanner.Scan()
    fs.lastErr = fs.scanner.Err()

    return hasScanned, fs.scanner.Text()
}

// Interpret the scanned line.  This sets the internal values.  If there was an
// error, return it.
func (fs *stdHexFormatScanner) interpretLine(line string) error {
    if strings.TrimSpace(line) == "" {
        return ErrBlankLine
    }

    offsetStr, line := consumeNonWhitespacePart(line)
    offset, err := strconv.ParseUint(offsetStr, 16, 64)
    if err != nil {
        return err
    }

    bytes := make([]byte, 0, fs.hexCols)

    for i := 0; i < fs.hexCols; i++ {
        var hexStr string 

        hexStr, line = consumeNonWhitespacePart(line)
        if hexStr == "" {
            break
        }

        hex, err := strconv.ParseUint(hexStr, 16, 32)
        if err != nil {
            break
        }

        bytes = append(bytes, byte(hex))
    }

    // TODO: Use the ASCII value
    fs.lastOffset = offset
    fs.lastData = bytes

    return nil
}

// Consumes the first non-whitespace part of a string.  Returns the part and the rest of the string
func consumeNonWhitespacePart(str string) (part string, rest string) {
    // Skip whitespaces
    tokLeft := -1
    tokRight := -1
    consumingToken := false

    for p, r := range str {
        if !consumingToken {
            if !unicode.IsSpace(r) {
                tokLeft = p
                consumingToken = true
            }
        } else {
            if unicode.IsSpace(r) {
                tokRight = p
                break
            }
        }
    }

    if consumingToken && (tokRight == -1) {
        tokRight = len(str)
    }

    if (tokLeft != -1) && (tokRight != -1) {
        part = str[tokLeft:tokRight]
        if tokRight < len(str) {
            rest = str[tokRight:]
        } else {
            rest = ""
        }
    } else {
        part = ""
        rest = str
    }

    return
}