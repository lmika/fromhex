package main

import (
    "io"
    "log"
)

// Writes the resules from a hexformat to an output stream
type hexWriter struct {
    Scanner     HexFormatScanner
    Writer      WriterWithOffset

    err         error
    firstLine   bool
    currOffset  uint64
}

// Writes the next scan.  Returns true if data was written
func (hw *hexWriter) writeNext() bool {
    hasNext := hw.Scanner.Scan()
    hw.err = hw.Scanner.Err()
    if !hasNext {
        return false
    }

    scannerOffset := hw.Scanner.Offset()
    scannerData := hw.Scanner.Bytes()

    // Assume the current offset based on what the scanner gives for the first read
    if hw.firstLine {
        hw.currOffset = scannerOffset
        hw.firstLine = false
    } else if scannerOffset != hw.currOffset {
        log.Printf("Warn: expected offset of %x, but was given an offset of %x", hw.currOffset, scannerOffset)
    }

    var n int
    n, hw.err = hw.Writer.Write(scannerOffset, scannerData)
    hw.currOffset += uint64(n)
    return true
}

// Writes the contents of a HexFormatScanner to the specific output.
// This continues until the hex scanner is exhausted.  It will then return the final
// error encountered
func WriteHexTo(scanner HexFormatScanner, output WriterWithOffset) error {
    hw := &hexWriter{
        Scanner: scanner,
        Writer: output,
        firstLine: true,
    }

    for hw.writeNext() { }
    return hw.err
}

// An interface for a writer which will receive offset reports
type WriterWithOffset interface {
    // Write data which was found at the given offset.
    Write(offset uint64, p []byte) (n int, err error)
}


// A wrapper for io.Writer
type IOWriter struct {
    W   io.Writer
}

func (w *IOWriter) Write(offset uint64, p []byte) (n int, err error) {
    return w.W.Write(p)
}


// A gate writer.  This will start writing to the underlying writer once the reported offset
// is greater than a given threshold
type LowerGateWriter struct {
    W       WriterWithOffset
    From    uint64
}

func (lgw *LowerGateWriter) Write(offset uint64, p []byte) (n int, err error) {
    err = nil
    n = len(p)

    offsetRight := offset + uint64(len(p))

    if offsetRight < lgw.From {
        // Do nothing
    } else if (offset < lgw.From) && (offsetRight >= lgw.From) {
        // Write the slice subset
        leftPoint := len(p) - int(offsetRight - lgw.From)
        log.Println(offset, offsetRight, lgw.From, "=", leftPoint)
        _, err = lgw.W.Write(offset + uint64(leftPoint), p[leftPoint:])
    } else {
        // Write the entire slice
        return lgw.W.Write(offset, p)
    }

    return
    //return w.W.Write(p)
}
