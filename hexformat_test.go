package main

import (
    "testing"
    "strings"
  . "github.com/seanpont/assert"
)

func TestConsumeNonWhitespace(t *testing.T) {
    assert := Assert(t)

    tok1, rest1 := consumeNonWhitespacePart("this is a test")
    assert.Equal(tok1, "this")
    assert.Equal(rest1, " is a test")

    tok2, rest2 := consumeNonWhitespacePart("  another  test  ")
    assert.Equal(tok2, "another")
    assert.Equal(rest2, "  test  ")

    tok3, rest3 := consumeNonWhitespacePart("    ")
    assert.Equal(tok3, "")
    assert.Equal(rest3, "    ")

    tok4, rest4 := consumeNonWhitespacePart("")
    assert.Equal(tok4, "")
    assert.Equal(rest4, "")
}

func TestConsumeNonWhitespaceInLoop(t *testing.T) {
    assert := Assert(t)

    tokens := make([]string, 0)

    for tok, rest := consumeNonWhitespacePart("  this is a line  ");
        tok != "";
        tok, rest = consumeNonWhitespacePart(rest) {
            tokens = append(tokens, tok)
    }

    assert.Equal(len(tokens), 4)
    assert.Equal(tokens[0], "this")
    assert.Equal(tokens[1], "is")
    assert.Equal(tokens[2], "a")
    assert.Equal(tokens[3], "line")
}

func TestInterpretLine(t *testing.T) {
    assert := Assert(t)
    hs := &stdHexFormatScanner{ hexCols: 4 }

    err := hs.interpretLine("000123 12 ad 8d 3f")
    assert.Nil(err)
    assert.Equal(hs.lastOffset, uint64(0x123))
    assert.Equal(hs.lastData, []byte { 0x12, 0xAD, 0x8D, 0x3F })


    err = hs.interpretLine("  05adf1 15 02 10 55  ")
    assert.Nil(err)
    assert.Equal(hs.lastOffset, uint64(0x5adf1))
    assert.Equal(hs.lastData, []byte { 0x15, 0x02, 0x10, 0x55 })


    err = hs.interpretLine("123456    88   44    ")
    assert.Nil(err)
    assert.Equal(hs.lastOffset, uint64(0x123456))
    assert.Equal(hs.lastData, []byte { 0x88, 0x44 })
}

func TestStdHexFormatScanner(t *testing.T) {
    assert := Assert(t)

    testStr := `
        00000  CA FE   BA BE
        00004  11 33   22 44

        00008  DA 4A   40 31
        0000C  15 43   21
    `

    fs := NewStandardHexFormatScanner(strings.NewReader(testStr))
    fs.(*stdHexFormatScanner).hexCols = 4

    assert.True(fs.Scan(), "Scanned")
    assert.Nil(fs.Err())
    assert.Equal(fs.Offset(), uint64(0x0))
    assert.Equal(fs.Bytes(), []byte { 0xCA, 0xFE, 0xBA, 0xBE })

    assert.True(fs.Scan(), "Scanned 2")
    assert.Nil(fs.Err())
    assert.Equal(fs.Offset(), uint64(0x4))
    assert.Equal(fs.Bytes(), []byte { 0x11, 0x33, 0x22, 0x44 })

    assert.True(fs.Scan(), "Scanned 3")
    assert.Nil(fs.Err())
    assert.Equal(fs.Offset(), uint64(0x8))
    assert.Equal(fs.Bytes(), []byte { 0xDA, 0x4A, 0x40, 0x31 })

    assert.True(fs.Scan(), "Scanned 4")
    assert.Nil(fs.Err())
    assert.Equal(fs.Offset(), uint64(0xC))
    assert.Equal(fs.Bytes(), []byte { 0x15, 0x43, 0x21 })

    assert.False(fs.Scan(), "Scanned 5")
    assert.Nil(fs.Err())
}